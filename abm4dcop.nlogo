;; Copyright Maxime Morge, 2022, 2023, 2024
extensions [ csv ]
;: Global variables
globals [
  debug                             ; true for having a trace
  maxNumberOfPosts                  ; the maximum number of post for one user
  numberOfQuestionsDistributionList ; the list of number of questions per user according to a distribution
  numberOfAnswersDistributionList   ; the list of number of questions per user according to a distribution
  idQuestion                        ; the ids of questions
  idAnswer                          ; the ids of answers
]

;; The agents
breed [users user] ; Turltes representing users
users-own [ ; the attributes of users
  numberOfQuestions                   ; the number of questions it can ask
  numberOfAnswers                     ; the number of answers it can reply
  numberOfPosts                       ; the number of posts it can publish
  numberOfQuestionsDuringCurrentTick  ; the number of questions it can ask per tick
  numberOfAnswersDuringCurrentTick    ; the number of answers it can reply per tick
  totalNumberOfQuestions
  totalNumberOfAnswers
  totalNumberOfPosts
  inDegree
  outDegree
  degree
]
breed [questions question] ; Turltes representing question
questions-own [ ; the attributes of questions
  id                                 ; the id of the question (1, 2, ...)
  timestamp                          ; the ticks when the question is asked
  author                             ; the asker
  isAnswered                         ; true if the question has an answer
  answerCount                        ; the number of answers
  score                              ; the score
]
breed [answers answer] ; Turltes representing question
answers-own [ ; the attributes of answers
  id                                 ; the id of the answer (1, 2, ...)
  timestamp                          ; the ticks when the answer is given
  author                             ; the helper
  replyTo                            ; the question
  interlocutor                       ; the author of the question
  score                              ; the score
  delay                              ; the number of ticks between the question and the answer
]

;; The interactions
directed-link-breed [helps help] ; A help is a directed link between users
directed-link-breed [authorships authorship] ; An authorship is a directed link between users and posts
directed-link-breed [replies reply] ; A reply is a directed link between posts
helps-own[
  weight
]

;; From an agentset of length l, reports the first n<=l agents
to-report take [n xs]
  report sublist xs 0 min list n (length xs)
end

;; Procedure for setup
to setup
  clear-all
  ask patches [set pcolor white]
  set-default-shape turtles "circle"
  populate
  reset-ticks
end

;; Procedure to create the users
to populate
  ; Initiate the global variables
  set debug false
  set idQuestion 1
  set idAnswer 1
  set maxNumberOfPosts (maxNumberOfQuestions + maxNumberOfAnswers)
  ; Setup of the bi-weekly numbers of questions
  setupBiWeeklyNumberOfQuestions

  ; Setup of the bi-weekly numbers of answers
  setupBiWeeklyNumberOfAnswers

  ; Setup users
  setupUsers
end

;; Procedure to setup of the bi-weekly numbers of questions
to setupBiWeeklyNumberOfQuestions
  set numberOfQuestionsDistributionList []
  let r  1 ; the rank of user
  foreach (range numberOfUsers)[
    let currentNumberOfQuestions 0
    if numberOfQuestionsDistribution = "Uniform" [ ; uniform distribution
      ; numberOfQuestions in [1/duration;maxNumberOfQuestions]
     set currentNumberOfQuestions (random (maxNumberOfQuestions)) + 1 / duration
    ]
    if numberOfQuestionsDistribution = "Normal" [
      ; normal distribution around the maxNumberOfQuestions / 2) with standard deviation maxNumberOfQuestions/6 in [1/duration;maxNumberOfQuestions]
      set currentNumberOfQuestions min (list max (list ( round ( random-normal  ( (maxNumberOfQuestions / 2 ))   (maxNumberOfQuestions / 6 ) ))  (1 / duration)) maxNumberOfQuestions )
    ]
    if numberOfQuestionsDistribution = "Power law" [
      let a maxNumberOfQuestions ; a depends on the maximum number of questions
      let k kNumberOfQuestions ; k is the scale factor
      let n numberOfUsers ; n is the number of users
      set currentNumberOfQuestions  (a * (r ^ k  - n ^ k) + 1 / duration)
      ; numberOfQuestions(r)  = a ( r^k - a.n^k )+ 1/duration
      ; with r in [1; n] is the rank of the user
      ; such that  numberOfQuestions(1) ~ maxNumberOfQuestions
      ; and numberOfQuestions(n) = 1/duration
    ]
    set numberOfQuestionsDistributionList insert-item length(numberOfQuestionsDistributionList) numberOfQuestionsDistributionList currentNumberOfQuestions
    set r (r + 1)
  ]
end

;; Procedure to setup of the bi-weekly numbers of answers
to setupBiWeeklyNumberOfAnswers
  set numberOfAnswersDistributionList []
  let r  1 ; the rank of user
  foreach (range numberOfUsers)[
   let currentNumberOfAnswers 0
    if numberOfAnswersDistribution = "Uniform" [ ; uniform distribution
      ; numberOfAnswers in [0;maxNumberOfAnswers]
     set currentNumberOfAnswers (random (maxNumberOfAnswers))
    ]
    if numberOfAnswersDistribution = "Normal" [
      ; normal distribution around the maxNumberOfAnswers / 2) with standard deviation maxNumberOfAnswers/6 in [0;maxNumberOfAnswers]
      set currentNumberOfAnswers min (list max (list ( round ( random-normal  ( (maxNumberOfAnswers / 2 ))   (maxNumberOfAnswers / 6 ) ) )  0) maxNumberOfAnswers )
    ]
    if numberOfAnswersDistribution = "Power law" [
      let a maxNumberOfAnswers ; a depends on the maximum number of answers
      let k kNumberOfAnswers ; k is the scale factor
      let n numberOfUsers ; n is the number of users
      set currentNumberOfAnswers  (a * (r ^ k  - n ^ k) )
      ; numberOfAnswers(r)  = a  (r^k - n^k)
      ; with r in [1; numberOfUsers] is the rank of the user
      ; where  numberOfAnswers(1) ~ maxNumberOfAnswers
      ; and numberOfAnswers(n) = 0
    ]
    set numberOfAnswersDistributionList insert-item length(numberOfAnswersDistributionList) numberOfAnswersDistributionList currentNumberOfAnswers
    set r (r + 1)
  ]
  set numberOfAnswersDistributionList reverse numberOfAnswersDistributionList
end

;; Procedure to setup users
to setupUsers
  create-users numberOfUsers
  ask users [ ; We assume that the more users asks question, the more user gives answers
    set numberOfQuestions (item who numberOfQuestionsDistributionList)
    set numberOfAnswers (item who numberOfAnswersDistributionList)
    set numberOfPosts ( numberOfQuestions + numberOfAnswers)
    set inDegree 0
    set outDegree 0
    ; draw the user
    set color 115
    ; set the size of the user depends on the activity
    set size (1.5 * (numberOfAnswers / maxNumberOfAnswers) + .2 )
    ; arange turtles in a circle sorted by size
    layout-circle sort-by [ [a b] -> [size] of a < [size] of b ]  turtles (world-width / 2 - 2)
  ]
end

;; Procedure to run the simulation
to go
  if ticks >= duration [stop] ; Maximum number of simulation step
  ; Setup round by updating the users' attribute
  ifelse concurrency = "no" [
    ask users [ ; foreach user
      go-user
    ] ; end of ask users
  ][
    ask-concurrent users [
      without-interruption [go-user]
      ]
  ]
  tick
  clear-all-plots
  updateLorenz
  updateScores
  updateNbAnswers
  updateMeanDelay
end

;; Procedure to run a user
to go-user
  let currentUser user who
  if debug [show (word "user "  currentUser)]
  ; the number of questions (s)he asks during the round
  set numberOfQuestionsDuringCurrentTick int numberOfQuestions ; is the integer part of numberOfQuestions
  if random-float 1 <= (numberOfQuestions - numberOfQuestionsDuringCurrentTick) [
    set numberOfQuestionsDuringCurrentTick numberOfQuestionsDuringCurrentTick + 1
  ] ; eventually plus one

  ; the number of answers (s)he replies during the round
  set numberOfAnswersDuringCurrentTick int numberOfAnswers ; is the integer part of numberOfAnswers
    if random-float 1 <= (numberOfAnswers - numberOfAnswersDuringCurrentTick) [
    set numberOfAnswersDuringCurrentTick numberOfAnswersDuringCurrentTick + 1
  ] ; eventually plus one

  ; Prepare to draw post
  let radius 15                         ; radius of the circle
  let tickThickness radius / duration   ; thickness of a tick
  let initGap (- ticks * tickThickness) ; shift the position of the posts wrt the user
  let gap initGap

  ; Question stage
  question-user initGap tickThickness

  ; Answer stage
  answer-user initGap tickThickness
end

;; Procedure for the question stage
to question-user [gap tickThickness]
    ; Create questions
  repeat numberOfQuestionsDuringCurrentTick [
      hatch-questions 1 [
        set id idQuestion
        set idQuestion (idQuestion + 1)
        set timestamp ticks
        set author myself
        set isAnswered false
        set answerCount 0
        create-authorship-to myself; create link to the author
        ask my-authorships [hide-link] ; hide this link
        set score (floor (maxQuestionScore * (id ^ kQuestionScore)))
        ; draw post
        forward gap
        set color red
        set size (2 *(score / maxQuestionScore) + 0.1) ; set the size of the question depends on the score
        set gap (gap - tickThickness / maxNumberOfPosts) ; move for the next post
     ]
    ]
  set totalNumberOfQuestions (totalNumberOfQuestions + numberOfQuestionsDuringCurrentTick)
end

;; Procedure for the answer stage
to answer-user [gap tickThickness]
  let currentUser user who
  let myQuestions in-authorship-neighbors
  let questionsFromOthers (questions with [not member? self myQuestions])
  ;;let orderedQuestionsFromOthers (take numberOfAnswersDuringCurrentTick (reverse sort-on [(score / ((answerCount + 1) ) ) ] questions) )
  ;;let orderedQuestionsFromOthers (take numberOfAnswersDuringCurrentTick ( (reverse sort-on [(score - 10 * answerCount )] (questions with [ (score - 10 * answerCount ) > 10]) ) ) )
  let orderedQuestionsFromOthers (take numberOfAnswersDuringCurrentTick ( (reverse sort-on [(1.5 * score / (2 ^ answerCount) )] questions ) ) )
  if debug [show (word "questions to answer "  orderedQuestionsFromOthers)]
  foreach(orderedQuestionsFromOthers)[
    selectedQuestion ->
    if debug [show (word "question to answer "  selectedQuestion) ]
    hatch-answers 1 [
      set id idAnswer
      set idAnswer (idAnswer + 1)
      set timestamp ticks
      set author myself
      set replyTo selectedQuestion
      set interlocutor [author] of replyTo
      let expectedScore ([(1.5 * ([score] of selectedQuestion) / ( 2 ^ (([answerCount] of selectedQuestion) + 1) ) ) ] of replyTo)
      set score (abs (random-normal expectedScore (0.5 * expectedScore)))
      set delay (timestamp - ([timestamp] of replyTo))
      ; update links
      create-authorship-to myself; create link to the author
      ask my-authorships [hide-link] ; hide this link
      if debug [show (word "replyTo"  replyTo)]
      ask replyTo [
        set isAnswered true
        set answerCount answerCount + 1
        create-reply-to myself; create link to the question;
      ]
      ask my-replies [hide-link] ; hide this link
      ask interlocutor [ ; create link between users with either create-help-to interlocutor or
        if debug [show (word "interlocutor" who)]
        ifelse in-link-neighbor? myself [ ; either a help link exists
         ; ask help ([who] of currentUser) who [
         ;   set weight (weight + 1)
         ;   set thickness (weight / 1000)
         ; ]
        ]
        [ ; or a new help link is created
         create-help-from currentUser [
            set weight 1
            set thickness (weight / 1000)
         ]
          set inDegree (inDegree + 1) ; inDegree of the asker is incremented
          ask currentUser [set outDegree (outDegree + 1)]; outDegree of the helper is incremented
        ] ; end of else
      ]
      ; draw post
      if debug [show (word "draw answer")]
      forward [gap] of myself
      set color green
      set size (2 * (score / maxQuestionScore) + 0.1) ; set the size of the question depends on the score
      set gap (gap - tickThickness / maxNumberOfPosts) ; move for the next post
    ]; end of hatch
    set totalNumberOfAnswers (totalNumberOfAnswers + 1)
  ]; end of foreach
set totalNumberOfPosts (totalNumberOfQuestions + totalNumberOfQuestions)
end

;; Procedure to plot the lorenz curves
to updateLorenz
  updatePercentageHelp
  updatePercentageAsk
  updatePercentageInDegree
  updatePercentageOutDegree
end

;; Procedure to draw percentageHelp
to updatePercentageHelp
  set-current-plot "percentageHelp"
  set-plot-y-range 0 1
  set-plot-x-range 0 1
  let totalNumberOfAnswersList ([totalNumberOfAnswers] of users)
  let sumTotalNumberOfAnswers sum totalNumberOfAnswersList
  set totalNumberOfAnswersList sort-by < totalNumberOfAnswersList
  let part 0
  let cumulativeTotalNumberOfAnswers 0
  foreach totalNumberOfAnswersList [
    x -> set cumulativeTotalNumberOfAnswers cumulativeTotalNumberOfAnswers + ( x / sumTotalNumberOfAnswers)
    set-current-plot-pen "answer"
    plotxy (part / numberOfUsers) cumulativeTotalNumberOfAnswers
    set-current-plot-pen "identity"
    plotxy (part / numberOfUsers) (part / numberOfUsers)
    set part part + 1
  ]
end

;; Procedure to draw percentageAsk
to updatePercentageAsk
  set-current-plot "percentageAsk"
  set-plot-y-range 0 1
  set-plot-x-range 0 1
  let totalNumberOfQuestionsList ([totalNumberOfQuestions] of users)
  let sumTotalNumberOfQuestions sum totalNumberOfQuestionsList
  set totalNumberOfQuestionsList sort-by < totalNumberOfQuestionsList
  let part 0
  let cumulativeTotalNumberOfQuestions 0
  foreach totalNumberOfQuestionsList [
    x -> set cumulativeTotalNumberOfQuestions cumulativeTotalNumberOfQuestions + ( x / sumTotalNumberOfQuestions)
    set-current-plot-pen "question"
    plotxy (part / numberOfUsers) cumulativeTotalNumberOfQuestions
    set-current-plot-pen "identity"
    plotxy (part / numberOfUsers) (part / numberOfUsers)
    set part part + 1
  ]
end

;;
to updatePercentageInDegree
  set-current-plot "percentageInDegree"
  set-plot-y-range 0 1
  set-plot-x-range 0 1
  let inDegreeList ([inDegree] of users)
  let sumInDegree sum inDegreeList
  set inDegreeList sort-by < inDegreeList
  let part 0
  let cumulativeInDegrees 0
  foreach inDegreeList [
    x -> set cumulativeInDegrees cumulativeInDegrees + ( x / sumInDegree)
    set-current-plot-pen "inDegree"
    plotxy (part / numberOfUsers) cumulativeInDegrees
    set-current-plot-pen "identity"
    plotxy (part / numberOfUsers) (part / numberOfUsers)
    set part part + 1
  ]
end

;; Procedure to draw percentageOutDegree
to updatePercentageOutDegree
  set-current-plot "percentageOutDegree"
  set-plot-y-range 0 1
  set-plot-x-range 0 1
  let inDegreeList ([inDegree] of users)
  let sumInDegree sum inDegreeList
  set inDegreeList sort-by < inDegreeList
  let outDegreeList ([outDegree] of users)
  let sumOutDegree sum outDegreeList
  set outDegreeList sort-by < outDegreeList
  let part 0
  let cumulativeOutDegrees 0
  foreach inDegreeList [
    x -> set cumulativeOutDegrees cumulativeOutDegrees + ( x / sumOutDegree)
    set-current-plot-pen "outDegree"
    plotxy (part / numberOfUsers) cumulativeOutDegrees
    set-current-plot-pen "identity"
    plotxy (part / numberOfUsers) (part / numberOfUsers)
    set part part + 1
  ]
end

;; Procedure to plot the scores
to updateScores
  updateScoreQuestion
  set-current-plot "scoreQuestion"
  set-plot-y-range 0 maxQuestionScore
  set-plot-x-range 1 (count questions)
  set-current-plot-pen "score"
  foreach (sort-on [id] questions) [
    q -> plotxy ([id] of q) ([score] of q)
  ]
  updateScoreAnswer
  set-current-plot "scoreAnswer"
  set-plot-y-range 0 maxQuestionScore
  set-plot-x-range 1 (count answers)
  set-current-plot-pen "score"
  foreach (sort-on [id] answers) [
    a -> plotxy ([id] of a) ([score] of a)
  ]
end

;; Procedure to draw scoreQuestion
to updateScoreQuestion
  set-current-plot "scoreQuestion"
  set-plot-y-range 0 maxQuestionScore
  set-plot-x-range 1 (count questions)
  set-current-plot-pen "score"
  foreach (sort-on [id] questions) [
    q -> plotxy ([id] of q) ([score] of q)
  ]
end

;; Procedure to drew scoreAnswer
to updateScoreAnswer
  set-current-plot "scoreAnswer"
  set-plot-y-range 0 maxQuestionScore
  set-plot-x-range 1 (count answers)
  set-current-plot-pen "score"
  foreach (sort-on [id] answers) [
    a -> plotxy ([id] of a) ([score] of a)
  ]
end

;; Procedure to draw nbAnswers
to updateNbAnswers
  set-current-plot "nbAnswers"
  set-plot-y-range -1  (max [answerCount] of questions)
  set-plot-x-range 1 (count questions)
  set-current-plot-pen "nbAnswers"
  foreach (sort-on [id] questions) [
    q -> plotxy ([id] of q) ([answerCount] of q)
  ]
end

;; Procedure to draw mean delay question
to updateMeanDelay
  set-current-plot "meanDelayQuestion"
  set-plot-y-range 0 ticks
  set-plot-x-range 0 ticks
  set-current-plot-pen "delay"
  foreach (range 0 ticks 1) [
    x ->
    let currentQuestions (questions with [isAnswered = true and timestamp = x])
    if (any? currentQuestions)[
      let meanDelay (mean [delay] of (answers with [member? replyTo currentQuestions]))
      plotxy x meanDelay
    ]
  ]
end

;; Procedure to report delays
to-report delays
  let data []
  foreach (sort-on [id] (questions with [isAnswered = true])) [
    q ->
    let x ([id] of q)
    let y (mean [delay] of (answers with [replyTo = q]))
    set data insert-item length(data) data (list x y)
  ]
  report csv:to-string fput ["id" "meanDelay"] data
end

;; Procedure to report scoreAnswers
to-report scoreAnswers
  let data []
  foreach (sort-on [id] answers) [
    a ->
    let x ([id] of a)
    let y ([score] of a)
    set data insert-item length(data) data (list x y)
  ]
  report csv:to-string fput ["idAndswer" "scoreAnswer"] data
end
@#$#@#$#@
GRAPHICS-WINDOW
337
10
1111
785
-1
-1
23.21212121212121
1
20
1
1
1
0
0
0
1
-16
16
-16
16
1
1
1
ticks
30.0

SLIDER
40
83
212
116
numberOfUsers
numberOfUsers
2
140225
1402.0
1
1
NIL
HORIZONTAL

SLIDER
31
216
282
249
maxNumberOfQuestions
maxNumberOfQuestions
1
200
61.0
1
1
NIL
HORIZONTAL

CHOOSER
31
168
312
213
numberOfQuestionsDistribution
numberOfQuestionsDistribution
"Power law" "Uniform" "Normal"
0

BUTTON
7
724
70
757
setup
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
188
723
251
756
go
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
86
723
171
756
one step
go
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
41
46
213
79
duration
duration
1
400
288.0
1
1
NIL
HORIZONTAL

SLIDER
30
254
288
287
kNumberOfQuestions
kNumberOfQuestions
-2
-0.01
-0.501
0.001
1
NIL
HORIZONTAL

CHOOSER
26
308
292
353
numberOfAnswersDistribution
numberOfAnswersDistribution
"Power law" "Uniform" "Normal"
0

SLIDER
26
398
284
431
kNumberOfAnswers
kNumberOfAnswers
-2
-0.001
-1.2
0.001
1
NIL
HORIZONTAL

SLIDER
25
357
284
390
maxNumberOfAnswers
maxNumberOfAnswers
1
200
167.3
1
1
NIL
HORIZONTAL

PLOT
1122
10
1412
200
percentageHelp
%age of users
%age of cum. sent answers 
0.0
1.0
0.0
1.0
true
false
"" ""
PENS
"answer" 1.0 0 -16777216 true "" ""
"identity" 1.0 0 -5298144 true "" ""

PLOT
1417
10
1675
201
percentageAsk
%age of users 
%age of cum. sent questions
0.0
1.0
0.0
1.0
true
false
"" ""
PENS
"question" 1.0 0 -16777216 true "" ""
"identity" 1.0 0 -5298144 true "" ""

PLOT
1122
207
1413
453
percentageOutDegree
%age of users
%age of cum. outDegree
0.0
1.0
0.0
1.0
true
false
"" ""
PENS
"outDegree" 1.0 0 -16777216 true "" ""
"identity" 1.0 0 -2674135 true "" ""

PLOT
1418
207
1675
449
percentageInDegree
%age of users
%age of cum. inDegrees
0.0
1.0
0.0
1.0
true
false
"" ""
PENS
"inDegree" 1.0 0 -16777216 true "" ""
"identity" 1.0 0 -5298144 true "" ""

PLOT
1125
461
1418
625
scoreQuestion
questions
scores
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"score" 1.0 0 -7500403 true "" ""

PLOT
1423
461
1677
621
scoreAnswer
answers
scores
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"score" 1.0 0 -7500403 true "" ""

SLIDER
24
451
196
484
maxQuestionScore
maxQuestionScore
0
5000
2500.0
1
1
NIL
HORIZONTAL

SLIDER
24
491
196
524
kQuestionScore
kQuestionScore
-2
0
-0.3
-0.05
1
NIL
HORIZONTAL

PLOT
1424
631
1678
825
meanDelayQuestion
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"delay" 1.0 0 -7500403 true "" ""

PLOT
1128
631
1419
824
nbAnswers
questions
nbAnswers
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"nbAnswers" 1.0 0 -7500403 true "" ""

CHOOSER
18
548
156
593
concurrency
concurrency
"yes" "no"
0

@#$#@#$#@
## WHAT IS IT?

This model simulates the global behavior of a Community of Practice (CoP), a group of people who share a concern, set of problems, or passion for a given topic, and who deepen their knowledge and expertise in that area through ongoing interacting on an ongoing basis. In this perspective, users ask questions and others answer. This simulation aims at understanding how users interact with each other.

## HOW IT WORKS

At each simulation step, users may send questions and reply with answers. The interaction is represented by a directed link from the helper to the asker.

The simulation can be setup with a number of simulation steps and a number of users.
Each step of the simulation represents 2 weeks. Each user is then endowed with a set of individual parameters that define their behavior:
- the average number of questions they post;
- the average number of answers they post;

## HOW TO USE IT

We set the following global parameters for a community of practice :

- the total number of users in the community. We consider a fixed size of the population;
- the bi-weekly maximum number of questions per user;
- the bi-weekly maximum number of answers per user;

The interface provides the ability to choose the distribution for the number of questions and the number of answers among uniform, normal, or power law distribution. kNumberOfQuestions and kNumberOfAnswers correspond to the power distribution’s scale of respectively the questions and answers.

The score of questions is a function of its position in time where
score = maxScorequestion * position ^ kQuestionScore


## THINGS TO NOTICE

The plots are:
- the barplot inDegreePlot represents the number of users having a certain in-degree (nb. answers of their questions);
- the barplot outDegreePlot represents the number of users having a certain out-degree (nb. of given answers);
- the barplot expertiseDistribution represents the number of users having a certain expertise level;
- the plot percentageAsk represents the cumulative asks of users from the lowest to the higest indegrees;
- the plot percentageHelp represents the cumulative helps of users from the lowest to the higest outdegrees.
- the plot scoreQuestions represents the score of questions from the first one to the last
- the plot scoreAnswers represents the score of questions from the first one to the last
- the plot delay Answers represents the mean delay of the answers for each question from the first one to the last

## THINGS TO TRY

After running the simulation for some number of steps, we can observe that most of the arrows are directed from/to the top region of the ring, which corresponds to users posting a large number of questions and answers (respectively at the right side and the left side of the region). Indeed, the red and the green dots corresponding resp. to questions and answers posted by a user are mostly concentrated in this region.

Indeed, more than 80% of users contribute to less than 20% of the community posted answers and questions. This is in compliance with the observations obtained from real insights on Stack Overflow.

## EXTENDING THE MODEL

The next step of the project will then consists  consists in identifying the appropriate way to generate Q&A scores to reflect users’ expertise. In that regard, it is also interesting to take into consideration how the expertise level of users evolves during their learning process. A reflection on how a user can choose which question to answer at any given step depending on their level of expertise can prove useful as well.

## NETLOGO FEATURES

N/A

## RELATED MODELS

See Wilensky, U. (1997). NetLogo Segregation model. http://ccl.northwestern.edu/netlogo/models/Segregation. Center for Connected Learning and Computer-Based Modeling, Northwestern University, Evanston, IL.

## CREDITS AND REFERENCES

Community Net Simulator: Using simulations to study online community networks.
Zhang, Jun, Ackerman, Mark S and Adamic, Lada. International Conference on 
Communities and Technologies 2007}, 295--321, 2007, Springer.

Copyright Amal Chaoui and Maxime Morge 2022
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.3.0
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
<experiments>
  <experiment name="RCommunity" repetitions="1" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <metric>scoreAnswers</metric>
    <enumeratedValueSet variable="duration">
      <value value="44"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="maxNumberOfAnswers">
      <value value="167.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="numberOfQuestionsDistribution">
      <value value="&quot;Power law&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="kNumberOfQuestions">
      <value value="-0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="kQuestionScore">
      <value value="-0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="numberOfAnswersDistribution">
      <value value="&quot;Power law&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="maxQuestionScore">
      <value value="2500"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="kNumberOfAnswers">
      <value value="-1.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="maxNumberOfQuestions">
      <value value="61"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="numberOfUsers">
      <value value="14022"/>
    </enumeratedValueSet>
  </experiment>
</experiments>
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
0
@#$#@#$#@
