# ABM4DCOP

Agent-Based Modeling for Distributed Community Of Practice

## Overview

ABM4DCOP is an agent-based model that explains the social phenomena that occur
on question-answer platforms such as Stack Overflow. This model
enables to understand the social dynamics that result from individual
behaviour in online communities. The formation and structuring of these
communities is a complex social phenomenon that involves the emergence of social
roles and the multiplication of cooperative links between users.

## Requirements

ABM4DCOP is built upon [NetLogo](https://ccl.northwestern.edu/netlogo/), a programming language and an IDE for agent-based modeling.

## Author

Maxime MORGE, 2022, 2023, 2024

## Additional contributors

Amal CHAOUI, Sébastien DELARRE, Fabien ELOIRE, Antoine NONGAILLARD

## License

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see http://www.gnu.org/licenses/.
